# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 19:53:20 2018

@author: wenhan
"""

import os
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def get_feature(file):
    with open(file, 'r') as f:
        data = json.load(f)

    df = pd.DataFrame(data['aggregate'], index=[0])
    df.drop(['method'], axis=1, inplace=True)
    return df


if __name__ == '__main__':
    
    files = os.listdir('features/')
    
    vmaf_list = []
    psnr_list = []
    ssim_list = []
    mssim_list = []
    
    for file in files:
        if file.startswith('VMAF'):
            vmaf_list.append(get_feature('features/' + file))
        elif file.startswith('PSNR'):
            psnr_list.append(get_feature('features/' + file))
        elif file.startswith('SSIM'):
            ssim_list.append(get_feature('features/' + file))
        elif file.startswith('MSSIM'):
            mssim_list.append(get_feature('features/' + file))
            
    vmaf = pd.concat(vmaf_list, ignore_index=True)
    psnr = pd.concat(psnr_list, ignore_index=True)
    ssim = pd.concat(ssim_list, ignore_index=True)
    mssim = pd.concat(mssim_list, ignore_index=True)
    
    # Combine all the features(VMAF, SSIM, MSSIM, PSNR)
    feature = vmaf.join([ssim, mssim, psnr])
    
    # save the feature
    # feature.to_csv('features.csv', index=False)
    
    # plot
    plt.figure()
    pd.plotting.scatter_matrix(feature.iloc[:, 7:11], diagonal='kde', figsize=(12,7))
    plt.show()
    
    plt.rcParams["figure.figsize"] = [12,5]
    feature.iloc[:, 11:-1].boxplot(rot=70)
#    plt.title('Raw Data (part)')
    plt.show()
        
    
    
    
    
    
    
    