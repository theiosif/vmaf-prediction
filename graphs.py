#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: Efia Prodan
"""
import parser_compact.parser as prs
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


PINK = '#FFAEAE'
BLUE = '#B4D8E7'
ORANGE = '#FF6600'
YELLOW = '#FFFF66'
GREEN = '#009933'
NBLUE = '#000099'
NORANGE = '#FF00BF'

# if you do not want to see and save all the demograpghic plots
# just set EXPORT_FIG to False

EXPORT_FIG = True


colnames1_1 = ['Gender']
colnames1_2 = ['Age']
colnames2 = ['Device', 'Activity', 'What', 'How_much', 'Part_day', 'When',
             'Frequency', 'Type_service', 'Profession', 'Continent', 'Age']


only_nums = ['NActivity',
             'NAge',
             'NContinent',
             'NDevice',
             'NFrequency',
             'NGen',
             'NPart_of_day',
             'NProfession',
             'NQuantity',
             'NService_type',
             'NTime',
             'NWhat']

length_dict = {'NActivity': 6,
               'NAge': 9,
               'NContinent': 5,
               'NDevice': 4,
               'NFrequency': 5,
               'NGen': 2,
               'NPart_of_day': 2,
               'NProfession': 10,
               'NQuantity': 6,
               'NService_type': 2,
               'NTime': 3,
               'NWhat': 16}


# In[]
# from parse file
results = prs.parse_results()
df = pd.DataFrame(results)
df1 = df.sort_values(by=['Step 2'][0])
# In[]
# Code by Iosif Andrei
FULLSCR = False
NORM = True
DONE = True

if FULLSCR:

    # force Python to use TkAgg backend
    B_END = plt.get_backend()
    if not (B_END == 'TkAgg'):
        plt.switch_backend('TkAgg')
        B_END = plt.get_backend()

    def max_fig(figure, backend=B_END):
        """
        UTILITY FUNCTION TO SHOW PLOTS IN FULLSCREEN
        (if buggy set FULLSCR == False
        and maximize the plots manually)
        """

        if B_END == 'TkAgg':

            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
            figure.show()  # close the figure to run the next section

        else:  # Just in case there's no TkAgg on the system
            figure.show()

else:
    def max_fig(figure):
        figure.show()

# In[]---------------------View Gendre/Age/Place------------------------------
gen = []
age = []
cont = []
prof = []

n_gen = []
n_age = []
n_cont = []
n_prof = []

type_str = []
freq = []
time = []
part_time = []
quant = []
what = []
activity = []
device = []

n_type_str = []
n_freq = []
n_time = []
n_part_time = []
n_quant = []
n_what = []
n_activity = []
n_device = []


for ix, row in df1.iterrows():
    list_gen = row['Step 2'][0]
    gen.append(list_gen)

    list_age = row['Step 2'][1]
    age.append(list_age)

    list_continent = row['Step 2'][2]
    cont.append(list_continent)

    list_prof = row['Step 2'][3]
    prof.append(list_prof)

for ix, row in df1.iterrows():
    list_type_str = row['Step 3'][0]
    type_str.append(list_type_str)

    list_freq = row['Step 3'][1]
    freq.append(list_freq)

    list_time = row['Step 3'][2]
    time.append(list_time)

    list_part_time = row['Step 3'][3]
    part_time.append(list_part_time)

    list_quant = row['Step 3'][4]
    quant.append(list_quant)

    list_what = row['Step 3'][5]
    what.append(list_what)

    list_activity = row['Step 3'][6]
    activity.append(list_activity)

    list_device = row['Step 3'][7]
    device.append(list_device)


data_2 = {'NAge': age, 'NContinent': cont, 'NProfession': prof,
          'NGen': gen, 'NService_type': type_str, 'NFrequency': freq,
          'NTime': time, 'NPart_of_day': part_time, 'NQuantity': quant,
          'NWhat': what, 'NActivity': activity, 'NDevice': device}
df_2 = pd.DataFrame(data=data_2)

for ix, row in df_2.iterrows():
    if (row.NGen == 1):
        n = 'Female'
        n_gen.append(n)
    elif (row.NGen == 2):
        n = 'Male'
        n_gen.append(n)

for ix, row in df_2.iterrows():
    if (row.NAge == 1):
        g = '<12'
        n_age.append(g)
    elif (row.NAge == 2):
        g = '12-17'
        n_age.append(g)
    elif (row.NAge == 3):
        g = '18-24'
        n_age.append(g)
    elif (row.NAge == 4):
        g = '25-34'
        n_age.append(g)
    elif (row.NAge == 5):
        g = '35-44'
        n_age.append(g)
    elif (row.NAge == 6):
        g = '45-54'
        n_age.append(g)
    elif (row.NAge == 7):
        g = '55-64'
        n_age.append(g)
    elif (row.NAge == 8):
        g = '65-74'
        n_age.append(g)
    elif (row.NAge == 9):
        g = '>75'
        n_age.append(g)

for ix, row in df_2.iterrows():
    if (row.NContinent == 1):
        c = 'America'
        n_cont.append(c)
    elif (row.NContinent == 2):
        c = 'Europe'
        n_cont.append(c)
    elif (row.NContinent == 3):
        c = 'Africa'
        n_cont.append(c)
    elif (row.NContinent == 4):
        c = 'Asia'
        n_cont.append(c)
    elif (row.NContinent == 5):
        c = 'Oceania'
        n_cont.append(c)

for ix, row in df_2.iterrows():
    if (row.NProfession == 1):
        p = 'Arts&entertainment'
        n_prof.append(p)
    elif (row.NProfession == 2):
        p = 'Business'
        n_prof.append(p)
    elif (row.NProfession == 3):
        p = 'Industrial&manufacturing'
        n_prof.append(p)
    elif (row.NProfession == 4):
        p = 'Law&Armed Forces'
        n_prof.append(p)
    elif (row.NProfession == 5):
        p = 'Science&technology'
        n_prof.append(p)
    elif (row.NProfession == 6):
        p = 'Healthcare&medicine'
        n_prof.append(p)
    elif (row.NProfession == 7):
        p = 'Service'
        n_prof.append(p)
    elif (row.NProfession == 8):
        p = 'Student'
        n_prof.append(p)
    elif (row.NProfession == 9):
        p = 'Unemployed'
        n_prof.append(p)
    elif (row.NProfession == 10):
        p = 'Other field'
        n_prof.append(p)

for ix, row in df_2.iterrows():
    if (row.NService_type == 1):
        p = 'Free'
        n_type_str.append(p)
    elif (row.NService_type == 2):
        p = 'Paid'
        n_type_str.append(p)

for ix, row in df_2.iterrows():
    if (row.NFrequency == 1):
        p = 'Extr. often'
        n_freq.append(p)
    elif (row.NFrequency == 2):
        p = 'Very often'
        n_freq.append(p)
    elif (row.NFrequency == 3):
        p = 'Moderately often'
        n_freq.append(p)
    elif (row.NFrequency == 4):
        p = 'Slightly often'
        n_freq.append(p)
    elif (row.NFrequency == 5):
        p = 'Not at all'
        n_freq.append(p)

for ix, row in df_2.iterrows():
    if (row.NTime == 1):
        p = 'Week'
        n_time.append(p)
    elif (row.NTime == 2):
        p = 'Weekend'
        n_time.append(p)
    elif (row.NTime == 3):
        p = 'No difference'
        n_time.append(p)

for ix, row in df_2.iterrows():
    if (row.NPart_of_day == 1):
        p = 'Morning'
        n_part_time.append(p)
    elif (row.NPart_of_day == 2):
        p = 'Evening'
        n_part_time.append(p)

for ix, row in df_2.iterrows():
    if (row.NQuantity == 1):
        p = 't<30min'
        n_quant.append(p)
    elif (row.NQuantity == 2):
        p = '30min<t<1h'
        n_quant.append(p)
    elif (row.NQuantity == 3):
        p = '1h<t<12h'
        n_quant.append(p)
    elif (row.NQuantity == 4):
        p = '2h<t<3h'
        n_quant.append(p)
    elif (row.NQuantity == 5):
        p = '3h<t<4h'
        n_quant.append(p)
    elif (row.NQuantity == 6):
        p = '4h<t'
        n_quant.append(p)

for ix, row in df_2.iterrows():
    if (row.NWhat == 1):
        p = 'Sports'
        n_what.append(p)
    elif (row.NWhat == 2):
        p = 'Game shows'
        n_what.append(p)
    elif (row.NWhat == 3):
        p = 'Science-Fiction '
        n_what.append(p)
    elif (row.NWhat == 4):
        p = 'Drama'
        n_what.append(p)
    elif (row.NWhat == 5):
        p = 'Talk shows'
        n_what.append(p)
    elif (row.NWhat == 6):
        p = 'Comedy'
        n_what.append(p)
    elif (row.NWhat == 7):
        p = 'News'
        n_what.append(p)
    elif (row.NWhat == 8):
        p = 'Thriller'
        n_what.append(p)
    elif (row.NWhat == 9):
        p = 'Reality shows'
        n_what.append(p)
    elif (row.NWhat == 10):
        p = 'Musical'
        n_what.append(p)
    elif (row.NWhat == 11):
        p = 'Action/adventure'
        n_what.append(p)
    elif (row.NWhat == 12):
        p = 'Romance'
        n_what.append(p)
    elif (row.NWhat == 13):
        p = 'Horror'
        n_what.append(p)
    elif (row.NWhat == 14):
        p = 'Documentary'
        n_what.append(p)
    elif (row.NWhat == 15):
        p = 'Cooking'
        n_what.append(p)
    elif (row.NWhat == 16):
        p = 'I do not know'
        n_what.append(p)

for ix, row in df_2.iterrows():
    if (row.NActivity == 1):
        p = 'Surfing web'
        n_activity.append(p)
    elif (row.NActivity == 2):
        p = 'Studying'
        n_activity.append(p)
    elif (row.NActivity == 3):
        p = 'Relaxing'
        n_activity.append(p)
    elif (row.NActivity == 4):
        p = 'Coocking'
        n_activity.append(p)
    elif (row.NActivity == 5):
        p = 'Go sleep'
        n_activity.append(p)
    elif (row.NActivity == 6):
        p = 'Traveling'
        n_activity.append(p)

for ix, row in df_2.iterrows():
    if (row.NDevice == 1):
        p = 'Tabelt'
        n_device.append(p)
    elif (row.NDevice == 2):
        p = 'TV'
        n_device.append(p)
    elif (row.NDevice == 3):
        p = 'Laptop'
        n_device.append(p)
    elif (row.NDevice == 4):
        p = 'Phone'
        n_device.append(p)

column_values1 = pd.Series(n_gen)
column_values2 = pd.Series(n_age)
column_values3 = pd.Series(n_cont)
column_values4 = pd.Series(n_prof)

column_values5 = pd.Series(n_type_str)
column_values6 = pd.Series(n_freq)
column_values7 = pd.Series(n_time)
column_values8 = pd.Series(n_part_time)
column_values9 = pd.Series(n_quant)
column_values10 = pd.Series(n_what)
column_values11 = pd.Series(n_activity)
column_values12 = pd.Series(n_device)

df_2.insert(loc=0, column='Gender', value=column_values1)
df_2.insert(loc=0, column='Age', value=column_values2)
df_2.insert(loc=0, column='Continent', value=column_values3)
df_2.insert(loc=0, column='Profession', value=column_values4)

df_2.insert(loc=0, column='Type_service', value=column_values5)
df_2.insert(loc=0, column='Frequency', value=column_values6)
df_2.insert(loc=0, column='When', value=column_values7)
df_2.insert(loc=0, column='Part_day', value=column_values8)
df_2.insert(loc=0, column='How_much', value=column_values9)
df_2.insert(loc=0, column='What', value=column_values10)
df_2.insert(loc=0, column='Activity', value=column_values11)
df_2.insert(loc=0, column='Device', value=column_values12)


df_2.insert(loc=0, column='VideoList', value=list(results.VideoList))
df_2.insert(loc=0, column='Ratings', value=list(results.Ratings))


# In[]--------------------- End of dataframe reconstr ------------------------


col1 = []
col2 = []
value = []
i = 0
a = len(df_2.columns)*2
fig = plt.figure()
if EXPORT_FIG:
    for name_col1 in colnames1_1:
        for name_col2 in colnames2:
            if (name_col1 != name_col2):
                val = df_2.groupby([name_col1,
                                    name_col2]).size().reset_index(
                                                        name="Values")
                piv = val.pivot(index=name_col2, columns=name_col1,
                                values='Values')
                val1 = piv.plot(kind='bar', color=[PINK, BLUE],
                                title=str(name_col1) + '/' + str(name_col2))
                plt.xticks(rotation=50, fontsize=5)
                max_fig(fig)
                if EXPORT_FIG:
                    plt.savefig(str(name_col1) + '-' +
                                str(name_col2) + str(i) + '.png')
                i += 1

    for name_col1 in colnames1_2:
        for name_col2 in colnames2:
            if (name_col1 != name_col2):
                fig = plt.figure()
                val = df_2.groupby([name_col1,
                                    name_col2]).size().reset_index(
                                                        name="Values")
                piv = val.pivot(index=name_col2, columns=name_col1,
                                values='Values')
                piv.plot(kind='bar', color=[PINK, BLUE, ORANGE, YELLOW,
                                            GREEN, NBLUE, NORANGE],
                         title=str(name_col1) + '/' + str(name_col2))
                plt.xticks(rotation=50, fontsize=5)
                max_fig(fig)
                if EXPORT_FIG:
                    plt.savefig(str(name_col1) + '-' +
                                str(name_col2) + str(i) + '.png')
                i += 1


# In[]:
df_nums = df_2[only_nums + ['VideoList', 'Ratings']]

xdf_dmos = pd.DataFrame(columns=['Selection', 'Value',
                                 'DMOS Matrix', 'Kick%'])

for col in only_nums:
    for val in range(length_dict[col]):
        df_select = df_nums.loc[df_nums[col] == (val + 1)]

        # Can't make a dmos out of <2 pepole, right?
        if len(df_select > 1):
            dmos, kick = prs.calc_dmos(df_select)
            if (kick < 1):
                xdf_dmos = xdf_dmos.append({'Selection': col,
                                            'Value': val+1,
                                            'DMOS Matrix': dmos,
                                            'Kick%': kick},
                                           ignore_index=True)
            else:
                xdf_dmos = xdf_dmos.append({'Selection': col,
                                            'Value': val+1,
                                            'DMOS Matrix': "BAD RATINGS",
                                            'Kick%': kick},
                                           ignore_index=True)

lst_dmos = []
lst_labels = []
fig1 = plt.figure()
fig2 = plt.figure()
for ix, row in xdf_dmos.iterrows():
    if type(row['DMOS Matrix']) != str:

        DMOS_df = pd.DataFrame(row['DMOS Matrix'],
                               index=[x[0] for x in prs.videos],
                               columns=['RP1', 'RP2', 'RP3', 'RP4'])

col1_1 = ['Crowd', 'ElFuente', 'FoxBird', 'Seeking', 'Boats', 'Cow', 'Food',
          'People', 'Crowd']
col1_2 = ['RP1', 'RP2', 'RP3', 'RP4']
plt.xticks(rotation=50, fontsize=5)
DMOS_df.boxplot(rot=50, fontsize=10, showfliers=True)
max_fig(fig1)
plt.savefig('Boxploting')
# label adjustment

DMOS_df.plot(kind='bar', rot=50, fontsize=5)
max_fig(fig2)
plt.savefig('Barploting')


# In[]
list_categ_dmos = ['Surfing web', 'Studying', 'Relaxing', 'Cooking',
                   'Going to sleep', 'Traveling', '18-24', '25-34',
                   '35-44', '45-54', '55-64', '65-74', '>75',
                   'America', 'Europe', 'Africa', 'Asia', 'Tablet', 'TV',
                   'Laptop', 'Phone', 'Extremely often', 'Very often',
                   'Moderately often', 'Slightly often', 'Not at all',
                   'Female', 'Male', 'Morning', 'Evening', 'Arts&entert.',
                   'Business', 'Industrial',
                   'Law Enforcement&Armed Forces', 'Sci&Tech', 'Health&med',
                   'Service', 'Student', 'Unemployed', 'Other field',
                   't<30min', '30min<t<1h', '1h<t<2h', '2h<t<3h', '3h<t<4h',
                   '>4h', 'Free', 'Paid', 'During the week', 'On weekends',
                   'No difference', 'Sports', 'Game shows', 'Science-Fiction',
                   'Drama', 'Talk shows', 'Comedy', 'News', 'Thriller',
                   'Reality shows', 'Musical', 'Action/adventure', 'Romance',
                   'Horror', 'Documentary', 'Cooking', "I don't know"]

column_values_dmos_categ = pd.Series(list_categ_dmos)
xdf_dmos.insert(loc=0, column='Categories', value=column_values_dmos_categ)

DMG_TYPE = ['NActivity', 'NAge', 'NContinent', 'NDevice', 'NFrequency', 'NGen',
            'NPart_of_day', 'NProfession', 'NQuantity', 'NService_type',
            'NTime', 'NWhat']
# list_means = []
# list_one_row = []
# rangen = ['0', '1', '2', '3']
# iloc_dmos = []
# list_means_plus_bad_bad = []
# list_means_plus_bad = np.zeros((67, 4))
# index = 0
# j = 0
# for demogr_type in DMG_TYPE:
#     mini_df = xdf_dmos.loc[xdf_dmos.Selection == demogr_type]
#     index = 0
#     for xi, row in mini_df.iterrows():
#         index += 1
#         j += 1
#         if type(row['DMOS Matrix']) != str:
#             iloc_dmos = mini_df.iloc[index-1]["DMOS Matrix"]
#             for i in range(0, 4):
#                 idx = int(i)
#                 mean = np.mean(iloc_dmos[:, idx])
#                 list_means_plus_bad[j-1][i] = mean
#         elif type(row['DMOS Matrix']) == str:
#             bad_rating = 'totally unreliable workers'
#             list_means_plus_bad_bad.append(bad_rating)
#
# # append the means results to our dataframe:)
# xdf_dmos['RP1'] = list_means_plus_bad[:, 0]
# xdf_dmos['RP2'] = list_means_plus_bad[:, 1]
# xdf_dmos['RP3'] = list_means_plus_bad[:, 2]
# xdf_dmos['RP4'] = list_means_plus_bad[:, 3]

# In[]:

# where do we have unusable ratings?

unusable = []

for _, row in xdf_dmos.iterrows():
    if row['DMOS Matrix'] == "BAD RATINGS":

        # SOME MISSING CATEGORIES, LOOK CAREFULLY AND ADD THEM
        unusable.append((row["Selection"], row["Categories"]))

# In[]:
# Averaging DMOS for reference of all trusty workers.
DMOS, percent_kicked_workers = prs.calc_dmos(results)

avg_all = np.zeros(4)
for i in range(4):
        avg_all[i] = np.mean(DMOS[:, i])

# keep only non-BAD RATINGS people
xdf_dmos_good = xdf_dmos.loc[xdf_dmos['DMOS Matrix'] != "BAD RATINGS"]

# here's where we'll only keep average DMOS
# (video content no longer matters)
xdf_dmos_avg = xdf_dmos_good.copy()

# pandas keeps old indices, so we need to get them
# to become  [0, 1, 2, etc] again.
xdf_dmos_avg.reset_index(drop=True, inplace=True)

avgs = np.zeros(4)

for ix, row in xdf_dmos_avg.iterrows():

    # average the ratings for all videos
    matrix = row["DMOS Matrix"]
    for i in range(4):
        avgs[i] = np.mean(matrix[:, i])

    xdf_dmos_avg["DMOS Matrix"][ix] = avgs


# In[]
RPs = ['RP1', 'RP2', 'RP3', 'RP4']
markers = ['.', 'o', 'v', '^', '<', '>', '1', 'x', '8', 'D', '+']
for_show = np.arange(1, 5)

j = 0
# get list w/ unique elements for selecting what gets plotted.
selector_type = list(set(list(xdf_dmos_avg.Selection.values)))
# STYLES1 = ['-']
# STYLES2 = ['--']
# STYLES3 = ['.']

for selector in selector_type:
    xdf_demogr = xdf_dmos_avg.loc[xdf_dmos_avg.Selection == selector]

    i = 0  # reset marker counter, why not
    fig = plt.figure()
    fig.suptitle(selector)
    # we want the real deal as comparison
    plt.scatter(for_show, avg_all, marker='*',
                label='Reference DMOS', c='r')
    dmos_mean = np.mean(avg_all)
    dmos_mean_r = round(dmos_mean, 4)
    print(dmos_mean)
    k = 0
    y_mean = 0
    for _, row in xdf_demogr.iterrows():
        k += 1
        y_mean = [np.mean(row["DMOS Matrix"])]*len(row["DMOS Matrix"])
        y_mean_r = round(y_mean[0], 4)
        print(y_mean[0])
        plt.scatter(for_show, row["DMOS Matrix"], marker=markers[i],
                    label=str(row["Categories"]))
        if (k == len(xdf_demogr)):
            mean_line = plt.plot(y_mean,
                                 label='Mean = '+str(y_mean_r),
                                 linestyle='-')
            dmos_mean = plt.plot(y_mean,
                                 label='Reference Mean = ' +
                                 str(dmos_mean_r),
                                 linestyle='--')
        i += 1
        i = i % (len(markers) - 2)

    fig.legend()
    plt.show()
    plt.savefig(str(j) + '.png')
    j += 1
    # break
