# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 02:36:27 2018

@author: wenhan
"""

import numpy as np
import pandas as pd
from scipy import stats

from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import LeaveOneOut

import matplotlib.pyplot as plt

#--------------------------- Load the data -----------------------------------#
df_dmos = pd.read_csv('docs/DMOS_export.csv')
df_dmos.set_index(df_dmos.columns[0], inplace=True)

dmos = df_dmos.values.reshape((32,1))
features = pd.read_csv('features.csv')

#----------------------- Leave One Out Cross Validation ----------------------#
loo = LeaveOneOut()
n = loo.get_n_splits(features)

# PCR
ERROR_pcr = []
for r in range(1,29):
    error = 0
    for train_index, test_index in loo.split(features):
        X_train, X_test = features.iloc[train_index], features.iloc[test_index]
        y_train, y_test = dmos[train_index], dmos[test_index]
        
        # normalize train data, test data
        mns, stds = X_train.mean(), X_train.std()
        X_train, X_test = (X_train - mns)/stds, (X_test - mns)/stds
        
        pca = PCA(n_components=r)
        lr = LinearRegression()
        model = lr.fit(pca.fit_transform(X_train), y_train)
        
        Y_predict = model.predict(pca.transform(X_test))
        e = np.square(Y_predict - y_test).mean()/n
        error += e
    ERROR_pcr.append(np.sqrt(error)) 

# PLSR
ERROR_plsr = []
for r in range(1,29):
    error = 0
    for train_index, test_index in loo.split(features):
        X_train, X_test = features.iloc[train_index], features.iloc[test_index]
        y_train, y_test = dmos[train_index], dmos[test_index]
        # normalize train data, test data
        mns, stds = X_train.mean(), X_train.std()
        X_train, X_test = (X_train - mns)/stds, (X_test - mns)/stds
        
        plsr = PLSRegression(n_components=r)
        plsr.fit(X_train, y_train)
        
        Y_predict = plsr.predict(X_test)
        e = np.square(Y_predict - y_test).mean()/n
        error += e
    ERROR_plsr.append(np.sqrt(error))
    
#-------- Plot: relationship between RMSE and number of components -----------#
plt.figure(figsize=(12,8))
plt.plot(range(1,29), ERROR_pcr, label='PCR')
plt.plot(range(1,29), ERROR_plsr, label='PLSR')
plt.xlabel('Number of Components')
plt.ylabel('Prediction Error/RMSE')
plt.title('Leave One Out')
plt.grid()
plt.legend(loc='upper left', ncol=1)
plt.show()

#------------------------ model and prediction -------------------------------#
n_pcr_best = ERROR_pcr.index(min(ERROR_pcr)) + 1
n_plsr_best = ERROR_plsr.index(min(ERROR_plsr)) + 1

# normalize the data
mns, stds = features.mean(), features.std()
features = (features - mns)/stds

# fit the model
pca = PCA(n_components=n_pcr_best)
lr = LinearRegression()
model_pcr = lr.fit(pca.fit_transform(features.iloc[:16,:]), dmos[:16])

model_plsr = PLSRegression(n_components=n_plsr_best)
model_plsr.fit(features.iloc[:16,:], dmos[:16])

# predict
dmos_pcr = model_pcr.predict(pca.transform(features.iloc[16:,:]))
dmos_plsr = model_plsr.predict(features.iloc[16:,:])

# RMSE
RMSE_pcr = np.sqrt(np.square(dmos[16:]-dmos_pcr).mean())
RMSE_plsr = np.sqrt(np.square(dmos[16:]-dmos_plsr).mean())
# PCC
PCC_plsr = stats.pearsonr(dmos[16:], dmos_plsr)[0]
PCC_pcr = stats.pearsonr(dmos[16:], dmos_pcr)[0]
# SRCC
SRCC_plsr = stats.spearmanr(dmos[16:], dmos_plsr)[0]
SRCC_pcr = stats.spearmanr(dmos[16:], dmos_pcr)[0]


# scatter plot
plt.figure()
plt.plot(dmos[16:], dmos_plsr, 'o')
plt.xlabel('DMOS')
plt.ylabel('Predicted DMOS')
plt.xlim((-0.5,3))
plt.ylim((-0.5,3))
plt.title('Model:PLSR, PCs:3, RMSE:0.533774581207, PCC:0.776514, SRCC:0.635294117647')
plt.grid()
plt.show()

plt.figure()
plt.plot(dmos[16:], dmos_pcr, 'o')
plt.xlabel('DMOS')
plt.ylabel('Predicted DMOS')
plt.xlim((-0.5,3))
plt.ylim((-0.5,3))
plt.title('Model:PCR, PCs:16, RMSE:2.13691203619, PCC:-0.106223, SRCC:-0.288235294118')
plt.grid()
plt.show()
