# -*- coding: utf-8 -*-
"""
Data Parsing Script

Collates functions to process
the .csv files from MTurk

@author: yoji
"""
import os
import numpy as np
import pandas as pd


# pylint: disable=E501

#
# Constants to fix stuff easily
#
REFERENCE = True  # do we want the raw 1_one.csv in the Var Explorer?
TRIM = 9  # how untrustworthy do we let our ratings be?

# Path adjustment in case
# we run the module separately
if __name__ == '__main__':

    print("parser run separately")
    FILEPATH = "../files_mturk/"
    PARENT = "../parser"
    EXPORT = True
else:
    print("parser imported.")
    FILEPATH = "files_mturk/"
    PARENT = ".."
    EXPORT = False


if REFERENCE:
    ONE = pd.read_csv(FILEPATH+"1_one.csv")

# ------- EXTRACTING QUESTIONS AND ANSWERS FOR DICTIONARY CREATION --------- #
QUESTIONS = pd.read_csv(FILEPATH+"0_survey_questions.csv")
QUESTIONS.answers = QUESTIONS.answers.str.split("; ")


def make_dicts():
    """
    Creates dictionaries
    for easier interpretation.
    """
    # Dictionaries for mapping int's to their literals for steps 2, 3
    dict_step_2 = []
    dict_step_3 = []

    for index, row in QUESTIONS.iterrows():
        localdict = {}
        listvals = row.answers
        listvals = [x.split(": ") for x in listvals]

        for item in listvals:
            localdict[int(item[0])] = item[1]

        if row.step == 2:
            dict_step_2.append(localdict)
        else:
            dict_step_3.append(localdict)

    #
    # Dictionary for mapping integers to literal strings
    dict_rating = {1: 'Bad',
                   2: 'Poor',
                   3: 'Fair',
                   4: 'Good',
                   5: 'Excellent'}

    return (dict_step_2, dict_step_3, dict_rating)


# ---------------------------------------------------------------------------#

def Result(i):
    """
    Proper naming requires
    no duplicate colnames
    """
    result = "Result " + str(i)

    return result

# In[]:
# ---------------------------------------------------------------------------#
# EASY INDEXING REQUIRES THIS

# Step Indexes for Videos
# (same in every vidlist)
vid1_ref = [10, 14]
vid1_1234 = [[11, 15],
             [16, 19],
             [17, 18],
             [20]]
#
vid2_ref = [12, 21]
vid2_1234 = [[13, 22, 26],
             [23, 27],
             [24],
             [25]]


# Adjust indexes for a list where
# step 10 -> index 0

vid1_ref = list(np.array(vid1_ref) - 10)
vid1_1234 = [list(np.array(x)-10) for x in np.array(vid1_1234)]

vid2_ref = list(np.array(vid2_ref) - 10)
vid2_1234 = [list(np.array(x)-10) for x in np.array(vid2_1234)]

# ---------------------------------------------------------------------------#
# Some nice lists and dicts

# Pairs
videos = [["Crowd", "ElFuente"],
          ["ElFuente", "FoxBird"],
          ["FoxBird", "Seeking"],
          ["Seeking", "Boats"],
          ["Boats", "Cow"],
          ["Cow", "Food"],
          ["Food", "People"],
          ["People", "Crowd"]]

# Dict to map back into names
dict1 = {1: "Crowd",
         2: "ElFuente",
         3: "FoxBird",
         4: "Boats",
         5: "Cow",
         6: "Food",
         7: "Seeking",
         8: "People"}
# ---------------------------------------------------------------------------#
# In[]:


def minimise(dataframe):
    """
    Converts data to numerical-only
    representation.
    """
    # list of indexes where numerical representation of data is stored
    # idx_id = [0, 1, 2, 3, 4, 5]  # worker ID, stuff like that
    idx_step_2 = [9, 11, 13, 15]
    idx_step_3 = list(np.arange(start=18, stop=34, step=2))
    idx_steps_stab = [36, 39, 42, 45]
    idx_steps_videos = list(np.arange(start=49, stop=103, step=3))
    idx_step_order = 101

    #
    # Indexes storing durations of steps
    idx_dur_15 = [6, 7, 16, 33, 34]
    idx_dur_s = [37, 40, 43, 46]
    idx_dur_v = list(np.arange(start=47, stop=99, step=3))

    colnames = ['Worker ID',
                'Begin Date',
                'Start Time',
                'End Date',
                'End Time',
                'Step 2',
                'Step 3',
                'Durations_15',
                'Stabilisation',
                'Duration_s',
                'Videos',
                'Duration_v',
                'Order']

    df_min = pd.DataFrame(columns=colnames)

    for _, row in dataframe.iterrows():
        if row[1] == 'Yes':
            df_min = df_min.append({'Worker ID': row[0],
                                    'Begin Date': row[2],
                                    'Start Time': row[3],
                                    'End Date': row[4],
                                    'End Time': row[5],
                                    'Step 2': list(row[idx_step_2]),
                                    'Step 3': list(row[idx_step_3]),
                                    'Durations_15': list(row[idx_dur_15]),
                                    'Stabilisation': list(row[idx_steps_stab]),
                                    'Duration_s': list(row[idx_dur_s]),
                                    'Videos': list(row[idx_steps_videos]),
                                    'Duration_v': list(row[idx_dur_v]),
                                    'Order': row[idx_step_order].split(' ')},
                                   ignore_index=True)
    return df_min

# ---------------------------------------------------------------------------#

# In[]:


def parse_results():
    """
    Parses the one.csv ... eight.csv files
    and returns a minimised version of them
    """
    df_list = []

    file_content = os.listdir(FILEPATH)
    os.chdir(FILEPATH)
    file_content.sort()
    file_content = file_content[1:9]

    for file in file_content:
        result = pd.read_csv(file, header=None)
        result = result.iloc[2:]  # strip "header"
        result.reset_index(inplace=True, drop=True)  # fix bad indexing

        result = minimise(result)

        result.rename(columns={'Videos': 'Ratings'}, inplace=True)
        result.drop('Stabilisation', axis=1, inplace=True)

        df_list.append(result)

    os.chdir(PARENT)  # move back into dir we came from

    # merge them and indicate video list origin
    result = pd.concat(df_list, keys=[1, 2, 3, 4, 5, 6, 7, 8])
    result = result.reset_index(level=0)
    result.rename(columns={"level_0": 'VideoList'},
                  inplace=True)  # fix the pandas shenanigans

    return result


# In[]
# ---------------------------------------------------------------------------#
# DMOS = Differential Mean Opinion Score
# DMOS = (1/N) * sum(1,N)[Rating(ref,n) - Rating(n)]

# Even though some subjects took part in multiple batches
# from the 8 available, I believe that rating quality can
# vary from test to test as well, not only from person to person.
# Therefore, DMOS was calculated per test batch, not globally
# despite the fact that every video appears in two batches.


def calc_dmos(results):

    DMOS1 = np.zeros((8, 4))  # will store DMOS per batch per vid.
    DMOS2 = np.zeros((8, 4))
    kicked_percent = 0

    for batch_no in range(1, 9):
        batch = results.loc[results.VideoList == batch_no]
        num_workers = len(batch)

        # Matrix of ratings
        ratings = np.array(batch.Ratings.tolist())
        # Ratings = NWorkers x 18 --> (2 vids x 9ratings)
        ratings_all = ratings.copy()

    # ----> We want to take out the shady people
        kicked = []

        # let's see how untrustworthy this guy's ratings are
        for worker in range(len(ratings_all)):
            indiv_ratings = ratings[worker]

            # assumption of innocence also works here :)
            mistrust = 0

            # check difference in reference ratings
            mistrust += abs(indiv_ratings[vid1_ref[0]] -
                            indiv_ratings[vid1_ref[1]])

            mistrust += abs(indiv_ratings[vid2_ref[0]] -
                            indiv_ratings[vid2_ref[1]])

            # for the duplicate RP's, we add (max-min)
            for dupe in vid1_1234:
                if len(dupe) > 1:
                    mistrust += max(dupe) - min(dupe)

            # The TRIM parameter was determined empirically
            # This is sometimes refered to as 'guesstimating'
            if (mistrust > TRIM):
                kicked.append(worker)

# ----> Voila, high quality ratings
        ratings = np.delete(ratings_all, kicked, axis=0)
        # nan's still snuck inside somehow
        ratings = np.delete(ratings,
                            [x for x in np.where(np.isnan(ratings))[0]], 0)

        num_ratings = len(ratings)

        if (num_ratings < 2):
            return 0, 1

#
# ----> Now we must have 1 rating per RP, therefore we'll average them
#                                       (the data is allegedly high-quality)

        # Average the reference ratings
        rate_1_ref = ((ratings[:, vid1_ref[0]]+ratings[:, vid1_ref[1]])) / 2
        rate_1_ref = [int(x) for x in rate_1_ref]

        rate_2_ref = ((ratings[:, vid2_ref[0]]+ratings[:, vid2_ref[1]])) / 2
        rate_2_ref = [int(x) for x in rate_2_ref]

        # Average the 1234 RP's and store them here
        rate_1_1234 = np.zeros((4, num_ratings))
        rate_2_1234 = np.zeros((4, num_ratings))

        # Oh man this will be excruciating.
        for place, ix in enumerate(vid1_1234):

            # do we have duplicate rate points?
            if len(ix) > 1:
                # if so, average them and round them down with int
                rate_avg = np.zeros(num_ratings)
                for sub_ix in ix:
                    rate_avg += ratings[:, sub_ix]

                # finish averaging and round
                rate_avg = [int(x/len(ix)) for x in rate_avg]

            else:
                rate_avg = ratings[:, ix[0]]  # I declared them as lists,
    #                                         # now I must pay the price.
            rate_1_1234[place] = rate_avg

    #

# -#-#-#-#-#-#-#-#- Do the Kansas City Shuffle for vid2 -#-#-#-#-#-#-#-#-#- #

        for place, ix in enumerate(vid2_1234):

            # do we have duplicate rate points?
            if len(ix) > 1:
                # if so, average them and round them down with int
                rate_avg = np.zeros(num_ratings)
                for sub_ix in ix:
                    rate_avg += ratings[:, sub_ix]

                # finish averaging and round
                rate_avg = [int(x/len(ix)) for x in rate_avg]

            else:
                rate_avg = ratings[:, ix[0]]  # I declared them as lists,
    #                                         # now I must pay the price.
            rate_2_1234[place] = rate_avg

            for rate_pt in range(4):
                DMOS1[batch_no-1][rate_pt] = sum(rate_1_ref -
                                                 rate_1_1234[rate_pt])
                DMOS1[batch_no-1][rate_pt] /= num_ratings

                DMOS2[batch_no-1][rate_pt] = sum(rate_2_ref -
                                                 rate_2_1234[rate_pt])
                DMOS2[batch_no-1][rate_pt] /= num_ratings

    # Let's see how many we kicked
        kicked_percent += len(kicked) / num_workers
    # It needs averaging
    kicked_percent /= 8
    kicked_percent = round(kicked_percent, 2)

#   # We got 2 DMOS's per video, so we gotta average them
    DMOS = np.zeros((8, 4))
    for idx in range(8):
        DMOS[idx] = (DMOS1[idx] + DMOS2[(idx + 1) % 8]) / 2

    print("\n\tpercent kicked: {} (TRIM = {})".format(kicked_percent, TRIM))

    return DMOS, kicked_percent

# ---------------------------------------------------------------------------#


# In[]:
def export_dmos(minimised_df, export_name):
    """
    Utility function to selectively output
    DMOS based on how we filter the data.
    """

    dmos_vals, kicked_percent = calc_dmos(minimised_df)
    DMOS_df = pd.DataFrame(dmos_vals, index=[x[0] for x in videos],
                           columns=['RP1', 'RP2', 'RP3', 'RP4'])

    if __name__ == '__main__':
        DMOS_df.to_csv("../docs/"+export_name+".csv")
    else:
        DMOS_df.to_csv("docs/"+export_name+".csv")


# In[]:

# Ignore this, it's for testing while
# writing the code. All useful functions
# are to be imported and used in main.py
if __name__ == '__main__':
    RUN_IT = True
    results = parse_results()
    dict_step_2, dict_step_3, dict_rating = make_dicts()
    DMOS, percent_kicked_workers = calc_dmos(results)

    DMOS_df = pd.DataFrame(DMOS, index=[x[0] for x in videos],
                           columns=['RP1', 'RP2', 'RP3', 'RP4'])

    if EXPORT:
        DMOS_df.to_csv("../docs/"+"DMOS_export.csv")
else:
    RUN_IT = False  # don't ask.
